from django import forms
from .models import CarWarehouse


class CarWarehouseForm(forms.ModelForm):
    class Meta:
        model = CarWarehouse
        fields = ('car_name', 'car_color')
