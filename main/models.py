from django.db import models


class CarWarehouse(models.Model):
    car_name = models.CharField(max_length=255, unique=True)
    COLOR = (
        ('R', 'Red'),
        ('B', 'Blue')
    )
    car_color = models.CharField(max_length=64, choices=COLOR)
    car_order_id = models.IntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return "(" + str(self.car_order_id) + ", " + self.car_name + ", " + self.get_car_color_display() + ")"
