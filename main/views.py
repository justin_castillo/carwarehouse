from django.shortcuts import render, redirect
from .models import CarWarehouse
from .forms import CarWarehouseForm


def index(request):
    all_cars = CarWarehouse.objects.all().order_by('-car_order_id')
    return render(request, 'main/index.html', {'all_cars': all_cars})


def store(request):
    all_cars = CarWarehouse.objects.all().order_by('-car_order_id')
    if request.method == "POST":
        form = CarWarehouseForm(request.POST)
        if form.is_valid():
            car = form.save(commit=False)
            car.car_order_id = CarWarehouse.objects.count()
            car.save()
            return redirect('store')
    else:
        form = CarWarehouseForm()
    return render(request, 'main/store.html', {'form': form, 'all_cars': all_cars})


def query(request):
    all_cars = CarWarehouse.objects.all().order_by('-car_order_id')
    if request.method == "POST":
        if request.POST.get("color") == 'Blue':
            all_cars = CarWarehouse.objects.filter(car_color="B").order_by('-car_order_id')
        elif request.POST.get("color") == 'Red':
            all_cars = CarWarehouse.objects.filter(car_color="R").order_by('-car_order_id')
        elif request.POST.get("color") == 'All':
            all_cars = CarWarehouse.objects.all().order_by('-car_order_id')
        return render(request, 'main/query.html', {'all_cars': all_cars})
    return render(request, 'main/query.html', {'all_cars': all_cars})


def move(request):
    all_cars = CarWarehouse.objects.all().order_by('-car_order_id')
    error_message = ''
    if request.method == "POST":
        try:
            move_car = CarWarehouse.objects.get(car_name=request.POST.get("move"))
            scoot_car = CarWarehouse.objects.get(car_name=request.POST.get("scoot"))

            start = move_car.car_order_id
            if move_car.car_order_id < scoot_car.car_order_id:
                end = scoot_car.car_order_id
                direction = 1
            else:
                end = scoot_car.car_order_id+1
                direction = -1

            for i in range(start, end, direction):
                x = CarWarehouse.objects.get(car_order_id=i)
                CarWarehouse.objects.get(car_order_id=i).delete()
                if start < end:
                    y = CarWarehouse.objects.get(car_order_id=i + 1)
                    CarWarehouse.objects.get(car_order_id=i + 1).delete()
                else:
                    y = CarWarehouse.objects.get(car_order_id=i - 1)
                    CarWarehouse.objects.get(car_order_id=i - 1).delete()

                u = CarWarehouse()
                u.car_name = x.car_name
                u.car_color = x.car_color
                u.car_order_id = y.car_order_id

                v = CarWarehouse()
                v.car_name = y.car_name
                v.car_color = y.car_color
                v.car_order_id = x.car_order_id

                u.save()
                v.save()

        except CarWarehouse.DoesNotExist:
            error_message = "Please enter a valid car name."
    return render(request, 'main/move.html', {'error_message': error_message, 'all_cars': all_cars})
