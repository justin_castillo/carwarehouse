from django.conf.urls import url
from . import views

urlpatterns = [
    # /
    url(r'^$', views.index, name='index'),

    # /store/
    url(r'^store/$', views.store, name='store'),

    # /query
    url(r'^query/$', views.query, name='query'),

    # /move
    url(r'^move/', views.move, name='move'),
]
